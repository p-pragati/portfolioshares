package com.hcl.user.service;

import com.hcl.user.exception.InvalidCredentialsException;

import com.hcl.user.model.User;

public interface IUserService {

	public String authenticate(String username, String password) throws InvalidCredentialsException;

	public User searchByUserId(int userId)throws InvalidCredentialsException;

}
