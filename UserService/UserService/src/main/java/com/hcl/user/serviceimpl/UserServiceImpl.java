package com.hcl.user.serviceimpl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.stereotype.Service;

import com.hcl.user.exception.InvalidCredentialsException;
import com.hcl.user.model.User;
import com.hcl.user.repository.UserRepository;
import com.hcl.user.service.IUserService;

@Service
public class UserServiceImpl implements IUserService {

	public final static Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);
	@Autowired
	UserRepository userRepository;

	@Override
	public String authenticate(String username, String password) throws InvalidCredentialsException {
		String methodName = "authenticate()";
		logger.info(methodName + "called");
		// TODO Auto-generated method stub
		User user = userRepository.findByUserNameAndPassword(username, password);
		if (user != null)
			return "Login success";
		logger.error("Invalid Credentials");
		throw new InvalidCredentialsException("Invalid Credentials!");
	}

	@Override
	public User searchByUserId(int userId) throws InvalidCredentialsException {
		// TODO Auto-generated method stub
		String methodName = "searchByUserId()";
		logger.info(methodName + "called");
		if(!userRepository.existsById(userId))
			throw new InvalidCredentialsException("UserId doesnt exist");
		else
		return userRepository.getById(userId);
	}

}
