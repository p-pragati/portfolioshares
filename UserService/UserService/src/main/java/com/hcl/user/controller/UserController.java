package com.hcl.user.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.user.dto.UserRequestDto;
import com.hcl.user.exception.InvalidCredentialsException;
import com.hcl.user.model.User;
import com.hcl.user.service.IUserService;

@RestController
public class UserController {

	@Autowired
	IUserService userService;

	@PostMapping("/users/login")
	public String login(@Valid @RequestBody UserRequestDto userRequestDto) throws InvalidCredentialsException {
		return userService.authenticate(userRequestDto.getUserName(), userRequestDto.getPassword());
	}
	

	
	@GetMapping("/users/{userId}")
	public ResponseEntity<User> getByUserId(@PathVariable int userId) throws InvalidCredentialsException  {
		return new ResponseEntity<User>(userService.searchByUserId(userId),HttpStatus.OK);
	}
}
