package com.hcl.user.dto;

import javax.validation.constraints.NotEmpty;

public class UserRequestDto {

	@NotEmpty(message = "userName should not be empty")
	private String userName;
	@NotEmpty(message = "password should not be less than 8 characters")
	private String password;

	public UserRequestDto(String userName, String password) {
		super();
		this.userName = userName;
		this.password = password;
	}

	public UserRequestDto() {

	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
}
