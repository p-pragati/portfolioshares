package com.hcl.user.service.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import com.hcl.user.dto.UserRequestDto;
import com.hcl.user.exception.InvalidCredentialsException;
import com.hcl.user.model.User;
import com.hcl.user.repository.UserRepository;
import com.hcl.user.serviceimpl.UserServiceImpl;



@SpringBootTest
public class UserServiceTests {
	
	@Mock
	UserRepository userRepository;
	
	@InjectMocks
	UserServiceImpl serviceImpl;
	
	static UserRequestDto userRequestDto;
	static User user;
	
	@BeforeAll
	public static void setUp(){
	userRequestDto=new UserRequestDto();
	userRequestDto.setUserName("Komal");
	userRequestDto.setPassword("ankita");
	
	user= new User();
	user.setUserName("Komal");
	user.setPassword("ankita");
	
	}
	
	@Test
	@DisplayName("authentication : positive scenario")
	public void authenticationTest() throws InvalidCredentialsException {
	//context
	when(userRepository.findByUserNameAndPassword("Komal", "ankita")).thenReturn(user);
	//event
	String result = serviceImpl.authenticate("Komal","ankita");
	//outcome
	assertEquals("Login success", result);
	
	}
	
	@Test
	@DisplayName("authentication : negative scenario")
	public void authenticationTest1() {
	//context
	when(userRepository.findByUserNameAndPassword("Komal", "ankita")).thenReturn(null);
	//event and outcome
	assertThrows(InvalidCredentialsException.class, ()->serviceImpl.authenticate("Komal", "ankita"));
	}

}
