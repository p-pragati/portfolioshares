package com.hcl.user.controller.test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.boot.test.context.SpringBootTest;


import com.hcl.user.controller.UserController;
import com.hcl.user.dto.UserRequestDto;
import com.hcl.user.exception.InvalidCredentialsException;
import com.hcl.user.model.User;
import com.hcl.user.serviceimpl.UserServiceImpl;

@SpringBootTest
public class UserControllerTests {

	@Mock
	UserServiceImpl userService;

	@InjectMocks
	UserController userController;

	static User user;
	static UserRequestDto userRequestDto;
	static UserRequestDto userRequestDtoNegative;

	@BeforeAll
	public static void setUp() {
		userRequestDto = new UserRequestDto();
		userRequestDto.setUserName("Pragati");
		userRequestDto.setPassword("password");
		


	}

	@Test
	@DisplayName("Login Function: Positive Scenario")
	public void loginTest() throws InvalidCredentialsException {
		// context
		when(userService.authenticate("Pragati", "password")).thenReturn("Login success");
		// event
		String result = userController.login(userRequestDto);
		// outcome
		assertEquals("Login success", result);
		
	}

	@Test
	@DisplayName("Login Function: Negative Scenario")
	public void loginTest2() throws InvalidCredentialsException {
		// context
		when(userService.authenticate("Pragati", "password")).thenThrow(InvalidCredentialsException.class);
		// event
		// outcome
		assertThrows(InvalidCredentialsException.class, () -> userController.login(userRequestDto));
	}

}
