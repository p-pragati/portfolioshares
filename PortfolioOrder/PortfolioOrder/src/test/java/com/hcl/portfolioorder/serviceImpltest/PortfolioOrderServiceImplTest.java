package com.hcl.portfolioorder.serviceImpltest;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.springframework.boot.test.context.SpringBootTest;

import com.hcl.portfolioorder.dto.AccountRequestDto;
import com.hcl.portfolioorder.dto.PortfolioOrderRequestDto;
import com.hcl.portfolioorder.dto.PortfolioOrderResponseDto;
import com.hcl.portfolioorder.dto.PortfolioRequestDto;
import com.hcl.portfolioorder.dto.PortfolioResponseDto;
import com.hcl.portfolioorder.exception.PortfolioOrderException;
import com.hcl.portfolioorder.feignclient.PortfolioConnect;
import com.hcl.portfolioorder.feignclient.UserConnect;
import com.hcl.portfolioorder.model.Account;
import com.hcl.portfolioorder.model.PortfolioOrder;
import com.hcl.portfolioorder.model.User;
import com.hcl.portfolioorder.repository.AccountRepository;
import com.hcl.portfolioorder.repository.PortfolioOrderRepository;
import com.hcl.portfolioorder.serviceImpl.PortfolioOrderServiceImpl;

import feign.FeignException;

@SpringBootTest
public class PortfolioOrderServiceImplTest {

	@Mock
	PortfolioOrderRepository portfolioOrderRepository;
	@Mock
	AccountRepository accountRepository;
	@Mock
	UserConnect userConnect;
	@Mock
	PortfolioConnect portfolioConnect;
	@InjectMocks
	PortfolioOrderServiceImpl portfolioOrderServiceImpl;

	static User user;
	static Account account;
	static AccountRequestDto accountRequestDto;
	static PortfolioOrder portfolioOrder;
	static PortfolioRequestDto portfolioRequestDto;
	static PortfolioResponseDto portfolioResponseDto;

	static PortfolioOrderRequestDto portfolioOrderRequestDto;
	static PortfolioOrderResponseDto portfolioOrderResponseDto;

	static List<AccountRequestDto> list;
	static List<Account> accountList;
	static List<PortfolioOrder> portfolioOrder1;
	static List<PortfolioRequestDto> portfolioRequestDtoList;
	static List<PortfolioOrderResponseDto> portfolioOrderList;

	@BeforeAll
	public static void setUp() {

		user = new User();
		user.setUserId(1);
		user.setUserName("vikky");
		user.setPassword("abc");
		user.setMobile_no(986754321);

		accountRequestDto = new AccountRequestDto();
		accountRequestDto.setAccountId(1);
		accountRequestDto.setAccountType("savings");
		accountRequestDto.setAccountBalance(30000);

		account = new Account();
		account.setAccountId(1);
		account.setUser(user);
		account.setAccountType("savings");
		account.setAccountBalance(20000);

		portfolioOrder = new PortfolioOrder();
		portfolioOrder.setPortfolioOrderId(1);
		portfolioOrder.setPortfolioName("hcl");
		portfolioOrder.setQuantity(1);
		portfolioOrder.setCurrentPrice(3000);
		portfolioOrder.setDateOfPurchase("09/07/21");
		portfolioOrder.setAccount(account);
		portfolioOrder.setUser(user);

		portfolioOrderResponseDto = new PortfolioOrderResponseDto();
		portfolioOrderResponseDto.setPortfolio(portfolioRequestDtoList);
		portfolioOrderResponseDto.setPortfolio_price(3000);

		portfolioRequestDto = new PortfolioRequestDto();
		portfolioRequestDto.setPortfolioName("hcl");
		portfolioRequestDto.setCurrentPrice(2000);
		portfolioRequestDto.setDateOfPurchase("09/07/21");

		portfolioRequestDtoList = new ArrayList<>();
		portfolioRequestDtoList.add(portfolioRequestDto);

		portfolioOrderRequestDto = new PortfolioOrderRequestDto();
		portfolioOrderRequestDto.setAccountId(1);
		portfolioOrderRequestDto.setPortfolioId(1);
		portfolioOrderRequestDto.setQuantity(1);

		portfolioResponseDto = new PortfolioResponseDto();
		portfolioResponseDto.setPortfolioId(1);
		portfolioResponseDto.setPortfolioName("hcl");
		portfolioResponseDto.setCurrentPrice(2000);

		list = new ArrayList<AccountRequestDto>();
		list.add(accountRequestDto);

		portfolioOrderList = new ArrayList<PortfolioOrderResponseDto>();
		portfolioOrderList.add(portfolioOrderResponseDto);

		accountList = new ArrayList<Account>();
		accountList.add(account);

		portfolioOrder1 = new ArrayList<PortfolioOrder>();
		portfolioOrder1.add(portfolioOrder);

	}

	@Test
	@DisplayName("searchAccountByUserId:positive scenerio")
	public void searchTest() throws FeignException, PortfolioOrderException {

		// context
		when(userConnect.searchByUserId(1)).thenReturn(user);
		when(accountRepository.existsById(1)).thenReturn(true);
		// event
//		BeanUtils.copyProperties(account, accountRequestDto);
		List<AccountRequestDto> result = portfolioOrderServiceImpl.findAccountByUserId(1);
		// outcome
		assertEquals(true, result);
	}

	@Test
	@DisplayName("searchAccountByUserId:Negative scenerio")
	public void searchTest1() {

		// context
		when(accountRepository.existsById(1)).thenReturn(false);
		// event

		// outcome
		assertThrows(PortfolioOrderException.class, () -> portfolioOrderServiceImpl.findAccountByUserId(1));
	}

	@Test
	@DisplayName("searchPortfolioByAccountId:positive scenerio")
	public void searchPortfolio() throws PortfolioOrderException {

		// context

		when(portfolioOrderRepository.existsById(1)).thenReturn(true);
		// event

		List<PortfolioOrderResponseDto> result = portfolioOrderServiceImpl.findPortfolioByAccountId(1);
		// outcome
		assertEquals(true, result);
	}

	@Test
	@DisplayName("searchPortfolioByAccountId:Negative scenerio")
	public void searchPortfolio1() {

		// context
		when(portfolioOrderRepository.existsById(1)).thenReturn(false);
		// event

		// outcome
		assertThrows(PortfolioOrderException.class, () -> portfolioOrderServiceImpl.findPortfolioByAccountId(1));
	}

	@Test
	@DisplayName("BuyPortfolio:positive scenerio")
	public void buyPortfolio() {

		// context

		when(accountRepository.existsById(1)).thenReturn(true);
		// event

		String result = portfolioOrderServiceImpl.buyPortfolio(portfolioOrderRequestDto);
		// outcome
		assertEquals(true, result);
	}
}
