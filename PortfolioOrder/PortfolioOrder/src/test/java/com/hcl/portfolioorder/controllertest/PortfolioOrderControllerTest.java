package com.hcl.portfolioorder.controllertest;

import static org.assertj.core.api.Assertions.assertThat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;

import com.hcl.portfolioorder.controller.PortfolioOrderController;
import com.hcl.portfolioorder.dto.AccountRequestDto;
import com.hcl.portfolioorder.dto.PortfolioOrderRequestDto;
import com.hcl.portfolioorder.dto.PortfolioOrderResponseDto;

import com.hcl.portfolioorder.exception.InvalidCredentialsException;
import com.hcl.portfolioorder.exception.PortfolioOrderException;
import com.hcl.portfolioorder.feignclient.PortfolioConnect;
import com.hcl.portfolioorder.model.Account;
import com.hcl.portfolioorder.model.PortfolioOrder;
import com.hcl.portfolioorder.model.User;
import com.hcl.portfolioorder.service.IPortfolioOrderService;

import feign.FeignException;

@SpringBootTest
public class PortfolioOrderControllerTest {

	@Mock
	IPortfolioOrderService portfolioOrderService;
	@Mock
	static PortfolioConnect portfolioConnect;

	@InjectMocks
	PortfolioOrderController portfolioOrderController;
	static User user;
	static Account account;
	static AccountRequestDto accountRequestDto;
	static PortfolioOrder portfolioOrder;

	static PortfolioOrderRequestDto portfolioOrderRequestDto;
	static PortfolioOrderResponseDto portfolioOrderResponseDto;

	static List<AccountRequestDto> list;
	static List<PortfolioOrderResponseDto> portfolioOrderList;

	@BeforeAll
	public static void setUp() {

		user = new User();
		user.setUserId(1);
		user.setUserName("vikky");
		user.setPassword("abc");
		user.setMobile_no(986754321);

		accountRequestDto = new AccountRequestDto();
		accountRequestDto.setAccountId(1);
		accountRequestDto.setAccountType("savings");
		accountRequestDto.setAccountBalance(30000);

		account = new Account();
		account.setAccountId(1);
		account.setUser(user);
		account.setAccountType("savings");
		account.setAccountBalance(20000);

		portfolioOrder = new PortfolioOrder();
		portfolioOrder.setPortfolioOrderId(1);
		portfolioOrder.setPortfolioName("hcl");
		portfolioOrder.setQuantity(1);
		portfolioOrder.setCurrentPrice(3000);
		portfolioOrder.setDateOfPurchase("09/07/21");
		portfolioOrder.setAccount(account);
		portfolioOrder.setUser(user);

		portfolioOrderResponseDto = new PortfolioOrderResponseDto();
//     portfolioOrderResponseDto.setPortfolio(portfolio);
		portfolioOrderResponseDto.setPortfolio_price(3000);

		portfolioOrderRequestDto = new PortfolioOrderRequestDto();
		portfolioOrderRequestDto.setAccountId(1);
		portfolioOrderRequestDto.setPortfolioId(1);
		portfolioOrderRequestDto.setQuantity(1);

		list = new ArrayList<AccountRequestDto>();
		list.add(accountRequestDto);

		portfolioOrderList = new ArrayList<PortfolioOrderResponseDto>();
		portfolioOrderList.add(portfolioOrderResponseDto);

	}

	@Test
	@DisplayName("Search AccountByUser:Positive Scenario")
	public void testDisplay() throws FeignException, PortfolioOrderException, InvalidCredentialsException {

//	BeanUtils.copyProperties(list, accountRequestDto);
		when(portfolioOrderService.findAccountByUserId(1)).thenReturn(list);
		System.out.println("----");
		ResponseEntity<List<AccountRequestDto>> result = portfolioOrderController.searchAccountByUserId(1);

		System.out.println(result);
		assertEquals(list, result.getBody());
		assertThat(result.getStatusCodeValue()).isEqualTo(200);
	}

	@Test
	@DisplayName("Search AccountByUser:Negative Scenario")
	public void testDisplay1() throws FeignException, PortfolioOrderException, InvalidCredentialsException {

		when(portfolioOrderService.findAccountByUserId(1)).thenThrow(PortfolioOrderException.class);

		assertThrows(PortfolioOrderException.class, () -> portfolioOrderController.searchAccountByUserId(1));

	}

	@Test
	@DisplayName("SearchPortfolioByAccount:Positive Scenario")
	public void testPortfolio() throws PortfolioOrderException {

		when(portfolioOrderService.findPortfolioByAccountId(1)).thenReturn(portfolioOrderList);
		ResponseEntity<List<PortfolioOrderResponseDto>> result = portfolioOrderController.searchPortfolioByAccountId(1);
		System.out.println(result);
		assertEquals(list, result.getBody());
		assertThat(result.getStatusCodeValue()).isEqualTo(200);
	}

	@Test
	@DisplayName("SearchPortfolioByAccount:Negative Scenario")
	public void testPortfolio1() throws PortfolioOrderException {

		when(portfolioOrderService.findPortfolioByAccountId(1)).thenThrow(PortfolioOrderException.class);

		assertThrows(PortfolioOrderException.class, () -> portfolioOrderController.searchPortfolioByAccountId(1));

	}

	@Test
	@DisplayName("BuyPortfolio:Positive Scenario")
	public void testBuyPortfolio() throws PortfolioOrderException {

		when(portfolioOrderService.buyPortfolio(portfolioOrderRequestDto)).thenReturn("Purchased Successful");

		ResponseEntity<String> result = portfolioOrderController.buyPortfolio(portfolioOrderRequestDto);
		System.out.println(result);
		assertEquals("Purchased Successful", result.getBody());
		assertThat(result.getStatusCodeValue()).isEqualTo(200);
	}

}
