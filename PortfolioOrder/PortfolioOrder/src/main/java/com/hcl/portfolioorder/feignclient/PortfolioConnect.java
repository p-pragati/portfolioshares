package com.hcl.portfolioorder.feignclient;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import com.hcl.portfolioorder.dto.PortfolioRequestDto;
import com.hcl.portfolioorder.dto.PortfolioResponseDto;
import com.hcl.portfolioorder.dto.PortfolioRequestDto;

@FeignClient(name = "portfolioservice", url = "http://localhost:8086")
public interface PortfolioConnect {

	@GetMapping("/portfolios/{portfolioId}")
	public PortfolioResponseDto getPortfolioById(@PathVariable int portfolioId);

}
