package com.hcl.portfolioorder.dto;

import javax.validation.constraints.NotNull;

public class UserRequestDto {

	@NotNull(message = "userName should not be empty")
	private String userName;

	@NotNull(message = "password should not be less than 8 characters ")
	private String password;

	public UserRequestDto(String userName, String password) {
		super();

		this.userName = userName;
		this.password = password;

	}

	public UserRequestDto() {

	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
