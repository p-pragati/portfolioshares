package com.hcl.portfolioorder.dto;

import java.util.List;

public class PortfolioOrderResponseDto {


	
	private double portfolio_price;
	private List<PortfolioRequestDto> portfolio;

	
	public double getPortfolio_price() {
		return portfolio_price;
	}

	public void setPortfolio_price(double portfolio_price) {
		this.portfolio_price = portfolio_price;
	}

	public List<PortfolioRequestDto> getPortfolio() {
		return portfolio;
	}

	public void setPortfolio(List<PortfolioRequestDto> portfolio) {
		this.portfolio = portfolio;
	}

	public PortfolioOrderResponseDto(int quantity, double portfolio_price,
			List<PortfolioRequestDto> portfolio) {
		super();
		
	
		this.portfolio_price = portfolio_price;
		this.portfolio = portfolio;
	}

	public PortfolioOrderResponseDto() {
	}
}
