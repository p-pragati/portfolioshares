package com.hcl.portfolioorder.exception;

public class PortfolioOrderException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	String message;


	public PortfolioOrderException(String message) {
		super(message);
		this.message = message;
	}


	public PortfolioOrderException() {
		super();
	}

}


