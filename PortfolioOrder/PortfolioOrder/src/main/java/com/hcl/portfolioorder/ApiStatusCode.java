package com.hcl.portfolioorder;

public class ApiStatusCode {
   
	public static final int TICKET_NOT_FOUND=401;
	
	public static final int INVALID_DATA=600;
	
}
