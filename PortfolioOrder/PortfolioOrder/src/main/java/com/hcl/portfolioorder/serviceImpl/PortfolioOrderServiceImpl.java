package com.hcl.portfolioorder.serviceImpl;

import java.util.ArrayList;


import java.util.List;

import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.portfolioorder.dto.AccountRequestDto;
import com.hcl.portfolioorder.dto.PortfolioOrderRequestDto;
import com.hcl.portfolioorder.dto.PortfolioOrderResponseDto;
import com.hcl.portfolioorder.dto.PortfolioRequestDto;
import com.hcl.portfolioorder.dto.PortfolioResponseDto;
import com.hcl.portfolioorder.dto.PortfolioRequestDto;

import com.hcl.portfolioorder.exception.PortfolioOrderException;
import com.hcl.portfolioorder.feignclient.PortfolioConnect;
import com.hcl.portfolioorder.feignclient.UserConnect;
import com.hcl.portfolioorder.model.Account;

import com.hcl.portfolioorder.model.PortfolioOrder;
import com.hcl.portfolioorder.repository.AccountRepository;
import com.hcl.portfolioorder.repository.PortfolioOrderRepository;
import com.hcl.portfolioorder.service.IPortfolioOrderService;
import com.hcl.portfolioorder.util.DateTimeUtil;

import feign.FeignException;

@Service
@Transactional
public class PortfolioOrderServiceImpl implements IPortfolioOrderService {

	public final static Logger logger = LoggerFactory.getLogger(PortfolioOrderServiceImpl.class);
	static double totalPrice = 0d;

	@Autowired
	PortfolioOrderRepository portfolioOrderRepository;

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	IPortfolioOrderService iPortfolioOrderService;

	@Autowired
	UserConnect userConnect;

	@Autowired
	PortfolioConnect portfolioConnect;

	public String buyPortfolio(PortfolioOrderRequestDto portfolioOrderRequestDto)    {
		String methodName = "buyPortfolio()";
		logger.info(methodName + "called");
		PortfolioOrder portfolioOrder = new PortfolioOrder();

//		BeanUtils.copyProperties(iPortfolioOrderService.findPortfolioByAccountId(portfolioOrderRequestDto.getAccountId()),portfolioOrder);

		PortfolioResponseDto portfolio=portfolioConnect.getPortfolioById(portfolioOrderRequestDto.getPortfolioId());
		System.out.println("Details of portfolio:" + portfolio.toString());
		Account account = accountRepository.getById(portfolioOrderRequestDto.getAccountId());
		System.out.println(account);

		portfolioOrder.setPortfolioName(portfolio.getPortfolioName());
		System.out.println(portfolio.getPortfolioName());
		portfolioOrder.setQuantity(portfolioOrderRequestDto.getQuantity());
		portfolioOrder.setDateOfPurchase(DateTimeUtil.date());
		portfolioOrder.setCurrentPrice(portfolio.getCurrentPrice());
		portfolioOrder.setAccount(account);
		portfolioOrder.setUser(account.getUser());

		portfolioOrderRepository.save(portfolioOrder);

		return "Purchased Successfully";

	}

	@Override
	public List<PortfolioOrderResponseDto> findPortfolioByAccountId(int accountId) throws PortfolioOrderException {
		// TODO Auto-generated method stub
//		if (portfolioOrderRepository.findPortfolioByAccountId(accountId) != null) {
//			throw new PortfolioOrderException("AccountId doesnt exist for this portfolio")
		String methodName = "findPortfolioByAccountId()";
		logger.info(methodName + "called");
		List<PortfolioOrder> portfolioOrderList = portfolioOrderRepository
				.findPortfolioOrderByAccountAccountId(accountId);

		List<PortfolioRequestDto> portfolioResponseDtoList = new ArrayList<>();
		logger.info(portfolioOrderList.toString());
		List<PortfolioOrderResponseDto> portfolioOrderResponseDtoList = new ArrayList<>();

		if (portfolioOrderList.isEmpty()) {
			logger.error("AccountId doesnt exist for this portfolio");
			throw new PortfolioOrderException("AccountId doesnt exist for this portfolio");

		}

		portfolioOrderList.forEach(portfolio -> {
			PortfolioRequestDto portfolioResponseDto = new PortfolioRequestDto();
			BeanUtils.copyProperties(portfolio, portfolioResponseDto);
			totalPrice += portfolio.getCurrentPrice() * portfolio.getQuantity();
			portfolioResponseDtoList.add(portfolioResponseDto);
		});
		portfolioOrderList.forEach(portfolio -> {
			PortfolioOrderResponseDto portfolioResponseDto = new PortfolioOrderResponseDto();
			portfolioResponseDto.setPortfolio(portfolioResponseDtoList);
			portfolioResponseDto.setPortfolio_price(totalPrice);

			portfolioOrderResponseDtoList.add(portfolioResponseDto);
		});

		return portfolioOrderResponseDtoList;

	}

	@Override
	public List<AccountRequestDto> findAccountByUserId(int userId) throws PortfolioOrderException, FeignException {
		// TODO Auto-generated method stub
		if (userConnect.searchByUserId(userId) == null) {
			logger.error("UserId doesnt exist for this account");
			throw new PortfolioOrderException("UserId doesnt exist for this account");
		}

		String methodName = "findAccountByUserId()";
		logger.info(methodName + "called");
		List<Account> accountList = accountRepository.findAccountByUserUserId(userId);
		List<AccountRequestDto> accountRequestDtoList = new ArrayList<>();

		for (Account account : accountList) {
			AccountRequestDto accountRequestDto = new AccountRequestDto();

			BeanUtils.copyProperties(account, accountRequestDto);
			accountRequestDtoList.add(accountRequestDto);

		}
		if (accountList.isEmpty()) {
			logger.error("AccountList is Empty");
			throw new PortfolioOrderException("AccountList is Empty");
		}

		return accountRequestDtoList;

	}
}
