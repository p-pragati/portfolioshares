package com.hcl.portfolioorder.model;

import javax.persistence.Entity;


import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;


@Entity
public class PortfolioOrder {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int portfolioOrderId;

	private String portfolioName;

	private String dateOfPurchase;

	private int quantity;

	private double currentPrice;


	@ManyToOne
	private Account account;
	@OneToOne
	private User user;

	public int getPortfolioOrderId() {
		return portfolioOrderId;
	}

	public void setPortfolioOrderId(int portfolioOrderId) {
		this.portfolioOrderId = portfolioOrderId;
	}

	public String getPortfolioName() {
		return portfolioName;
	}

	public void setPortfolioName(String portfolioName) {
		this.portfolioName = portfolioName;
	}

	public String getDateOfPurchase() {
		return dateOfPurchase;
	}

	public void setDateOfPurchase(String dateOfPurchase) {
		this.dateOfPurchase = dateOfPurchase;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	
	public double getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}

	public Account getAccount() {
		return account;
	}

	public void setAccount(Account account) {
		this.account = account;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	

	public PortfolioOrder(int portfolioOrderId, String portfolioName, String dateOfPurchase, int quantity, double currentPrice, Account account, User user) {
		super();
		this.portfolioOrderId = portfolioOrderId;
		this.portfolioName = portfolioName;
		this.dateOfPurchase = dateOfPurchase;
		this.quantity = quantity;
		this.currentPrice = currentPrice;
		this.account = account;
		this.user = user;
	}

	public PortfolioOrder() {

	}

	@Override
	public String toString() {
		return "PortfolioOrder [portfolioOrderId=" + portfolioOrderId + ", portfolioName=" + portfolioName
				+ ", dateOfPurchase=" + dateOfPurchase + ", quantity=" + quantity + ", currentPrice=" + currentPrice
				+ ", account=" + account + ", user=" + user + "]";
	}

	
}
