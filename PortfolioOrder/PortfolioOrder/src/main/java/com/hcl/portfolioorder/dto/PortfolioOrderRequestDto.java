package com.hcl.portfolioorder.dto;

public class PortfolioOrderRequestDto {

	private int accountId;
	private int portfolioId;
	private int quantity;

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public int getPortfolioId() {
		return portfolioId;
	}

	public void setPortfolioId(int portfolioId) {
		this.portfolioId = portfolioId;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public PortfolioOrderRequestDto(int accountId, int portfolioId, int quantity) {
		super();

		this.accountId = accountId;
		this.portfolioId = portfolioId;
		this.quantity = quantity;

	}

	public PortfolioOrderRequestDto() {
	}

	@Override
	public String toString() {
		return "PortfolioOrderRequestDto [accountId=" + accountId + ", portfolioId=" + portfolioId + ", quantity="
				+ quantity + "]";
	}
	
}
