package com.hcl.portfolioorder.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.portfolioorder.dto.AccountRequestDto;
import com.hcl.portfolioorder.dto.PortfolioOrderRequestDto;
import com.hcl.portfolioorder.dto.PortfolioOrderResponseDto;

import com.hcl.portfolioorder.dto.PortfolioResponseDto;

import com.hcl.portfolioorder.dto.UserRequestDto;
import com.hcl.portfolioorder.exception.InvalidCredentialsException;
import com.hcl.portfolioorder.exception.PortfolioOrderException;
import com.hcl.portfolioorder.feignclient.PortfolioConnect;
import com.hcl.portfolioorder.feignclient.UserConnect;
import com.hcl.portfolioorder.serviceImpl.PortfolioOrderServiceImpl;

import feign.FeignException;

@RestController
public class PortfolioOrderController {

	@Autowired
	UserConnect userConnect;

	@Autowired
	PortfolioConnect portfolioConnect;

	@Autowired
	PortfolioOrderServiceImpl portfolioOrderServiceImpl;

	@PostMapping("/users/login")
	public String login(@RequestBody UserRequestDto userRequestDto) {
		return userConnect.authenticate(userRequestDto);
	}

	@GetMapping("/users/{userId}/accounts")
	public ResponseEntity<List<AccountRequestDto>> searchAccountByUserId(@PathVariable int userId)
			throws PortfolioOrderException, FeignException, InvalidCredentialsException {
		return new ResponseEntity<List<AccountRequestDto>>(portfolioOrderServiceImpl.findAccountByUserId(userId),
				HttpStatus.OK);
	}

	@GetMapping("/portfolios/{portfolioId}")
	public ResponseEntity<PortfolioResponseDto> getPortfolioById(@PathVariable int portfolioId) {
		return new ResponseEntity<PortfolioResponseDto>(portfolioConnect.getPortfolioById(portfolioId), HttpStatus.OK);
	}

	@GetMapping("/accounts/{accountId}/portfolios")
	public ResponseEntity<List<PortfolioOrderResponseDto>> searchPortfolioByAccountId(@PathVariable int accountId)
			throws PortfolioOrderException {
		return new ResponseEntity<List<PortfolioOrderResponseDto>>(
				portfolioOrderServiceImpl.findPortfolioByAccountId(accountId), HttpStatus.OK);
	}

	@PostMapping("/portfolioorders")
	public ResponseEntity<String> buyPortfolio(@RequestBody PortfolioOrderRequestDto portfolioOrderRequestDto) {
		return new ResponseEntity<String>(portfolioOrderServiceImpl.buyPortfolio(portfolioOrderRequestDto),
				HttpStatus.OK);
	}

}
