package com.hcl.portfolioorder.feignclient;

import org.springframework.cloud.openfeign.FeignClient;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import com.hcl.portfolioorder.dto.UserRequestDto;
import com.hcl.portfolioorder.model.User;

@FeignClient(name = "userservice", url = "http://localhost:8085")

public interface UserConnect {

	@PostMapping("/users/login")
	public String authenticate(@RequestBody UserRequestDto userRequestDto);

	@GetMapping("/users/{userId}")
	public User searchByUserId(@PathVariable int userId);

}
