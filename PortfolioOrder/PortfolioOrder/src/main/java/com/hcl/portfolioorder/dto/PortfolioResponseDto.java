package com.hcl.portfolioorder.dto;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PortfolioResponseDto {
	
	private int portfolioId;
	@NotEmpty(message = "portfolioName should not be empty")
	private String portfolioName;
	
	
	private double currentPrice;


	public int getPortfolioId() {
		return portfolioId;
	}

	public void setPortfolioId(int portfolioId) {
		this.portfolioId = portfolioId;
	}

	public String getPortfolioName() {
		return portfolioName;
	}

	public void setPortfolioName(String portfolioName) {
		this.portfolioName = portfolioName;
	}



	public double getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}

	public PortfolioResponseDto( int portfolioId,String portfolioName, double currentPrice) {
		super();
		this.portfolioId=portfolioId;
		this.portfolioName = portfolioName;
	
		this.currentPrice = currentPrice;
	}
	public PortfolioResponseDto() {
		
	}

	@Override
	public String toString() {
		return "PortfolioResponseDto [portfolioId=" + portfolioId + ", portfolioName=" + portfolioName
				+ ", currentPrice=" + currentPrice + "]";
	}

}



