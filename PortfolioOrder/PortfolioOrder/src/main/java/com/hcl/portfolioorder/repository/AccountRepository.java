package com.hcl.portfolioorder.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.portfolioorder.model.Account;

@Repository
public interface AccountRepository extends JpaRepository<Account, Integer> {

	public List<Account> findAccountByUserUserId(int userId);

}
