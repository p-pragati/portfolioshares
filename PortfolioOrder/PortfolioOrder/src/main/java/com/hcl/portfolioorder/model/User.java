package com.hcl.portfolioorder.model;

import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;

@Entity
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private int userId;

	@NotNull(message = "userName should not be empty")
	private String userName;

	@NotNull(message = "password should not be less than 8 characters ")
	private String password;

	@NotNull(message = "mobile_no should not be less than 10")
	private long mobile_no;

	public User(int userId, String userName, String password, long mobile_no) {
		super();
		this.userId = userId;
		this.userName = userName;
		this.password = password;
		this.mobile_no = mobile_no;
	}

	public User() {

	}

	public int getUserId() {
		return userId;
	}

	public void setUserId(int userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public long getMobile_no() {
		return mobile_no;
	}

	public void setMobile_no(long mobile_no) {
		this.mobile_no = mobile_no;
	}

	@Override
	public String toString() {
		return "User [userId=" + userId + ", userName=" + userName + ", password=" + password + ", mobile_no="
				+ mobile_no + "]";
	}

}
