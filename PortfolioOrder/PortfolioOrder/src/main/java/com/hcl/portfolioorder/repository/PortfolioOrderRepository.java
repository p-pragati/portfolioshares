package com.hcl.portfolioorder.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.hcl.portfolioorder.model.PortfolioOrder;

@Repository
@Transactional
public interface PortfolioOrderRepository extends JpaRepository<PortfolioOrder, Integer> {

	public List<PortfolioOrder> findPortfolioOrderByAccountAccountId(int accountId);

}
