package com.hcl.portfolioorder.dto;

public class PortfolioRequestDto {


	private String portfolioName;

	private double currentPrice;
	private String dateOfPurchase;

	

	public String getPortfolioName() {
		return portfolioName;
	}

	public void setPortfolioName(String portfolioName) {
		this.portfolioName = portfolioName;
	}

	

	public double getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}

	public String getDateOfPurchase() {
		return dateOfPurchase;
	}

	public void setDateOfPurchase(String dateOfPurchase) {
		this.dateOfPurchase = dateOfPurchase;
	}

	
	

	
	@Override
	public String toString() {
		return "PortfolioResponseDto [portfolioName=" + portfolioName + ", currentPrice=" + currentPrice
				+ ", dateOfPurchase=" + dateOfPurchase + "]";
	}

	public PortfolioRequestDto(String portfolioName, double currentPrice, String dateOfPurchase) {
		super();
		this.portfolioName = portfolioName;
	
		this.currentPrice = currentPrice;
		this.dateOfPurchase = dateOfPurchase;
	}

	public PortfolioRequestDto() {
	}
}
