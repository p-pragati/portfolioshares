package com.hcl.portfolioorder.dto;

import javax.validation.constraints.Size;

public class AccountRequestDto {

	private int accountId;
	private String accountType;
	@Size(message = "should be greater than 0")
	private double accountBalance;

	public int getAccountId() {
		return accountId;
	}

	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}

	public String getAccountType() {
		return accountType;
	}

	public void setAccountType(String accountType) {
		this.accountType = accountType;
	}

	public double getAccountBalance() {
		return accountBalance;
	}

	public void setAccountBalance(double accountBalance) {
		this.accountBalance = accountBalance;
	}

	public AccountRequestDto(int accountId, String accountType,
			@Size(message = "should be greater than 0") double accountBalance) {
		super();
		this.accountId = accountId;
		this.accountType = accountType;
		this.accountBalance = accountBalance;
	}

	public AccountRequestDto() {
	}

	@Override
	public String toString() {
		return "AccountRequestDto [accountId=" + accountId + ", accountType=" + accountType + ", accountBalance="
				+ accountBalance + "]";
	}

}
