package com.hcl.portfolioorder.service;

import java.util.ArrayList;

import java.util.List;

import org.springframework.beans.BeanUtils;

import com.hcl.portfolioorder.dto.AccountRequestDto;
import com.hcl.portfolioorder.dto.PortfolioOrderRequestDto;
import com.hcl.portfolioorder.dto.PortfolioOrderResponseDto;
import com.hcl.portfolioorder.exception.InvalidCredentialsException;
import com.hcl.portfolioorder.exception.PortfolioOrderException;

import com.hcl.portfolioorder.model.PortfolioOrder;

import feign.FeignException;

public interface IPortfolioOrderService {

	public List<AccountRequestDto> findAccountByUserId(int userId) throws PortfolioOrderException, FeignException;

	public String buyPortfolio(PortfolioOrderRequestDto portfolioOrderRequestDto)  ;

	public List<PortfolioOrderResponseDto> findPortfolioByAccountId(int accountId) throws PortfolioOrderException;

	public static List<PortfolioOrderRequestDto> convertPortfolioOrdertoPortfolioOrderRequestDto(
			List<PortfolioOrder> portfolioOrderList) {
		List<PortfolioOrderRequestDto> portfolioOrderRequestDtoList = new ArrayList<PortfolioOrderRequestDto>();
		portfolioOrderList.forEach(portfolioOrder -> {
			PortfolioOrderRequestDto portfolioRequestDto = new PortfolioOrderRequestDto();
			BeanUtils.copyProperties(portfolioOrder, portfolioRequestDto);
			portfolioOrderRequestDtoList.add(portfolioRequestDto);
		});

		return portfolioOrderRequestDtoList;
	}

}
