package com.hcl.portfolio.service;

import java.util.List;

import com.hcl.portfolio.dto.PortfolioResponseDto;
import com.hcl.portfolio.exception.PortfolioException;
import com.hcl.portfolio.model.Portfolio;

public interface IPortfolioService {

	public List<Portfolio> getPortfolio();

	public PortfolioResponseDto searchPortfolioById(int portfolioId) throws PortfolioException ;

}
