package com.hcl.portfolio.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class PortfolioRequestDto {

	private int portfolioId;
	@NotNull(message = "portfolioName should not be empty")
	private String portfolioName;

	@Size(message = "size must be greater than 0")
	private double currentPrice;

	public int getPortfolioId() {
		return portfolioId;
	}

	public void setPortfolioId(int portfolioId) {
		this.portfolioId = portfolioId;
	}

	public String getPortfolioName() {
		return portfolioName;
	}

	public void setPortfolioName(String portfolioName) {
		this.portfolioName = portfolioName;
	}

	public double getCurrentPrice() {
		return currentPrice;
	}

	public void setCurrentPrice(double currentPrice) {
		this.currentPrice = currentPrice;
	}

	public PortfolioRequestDto(int portfolioId, String portfolioName, double currentPrice) {
		super();
		this.portfolioId = portfolioId;
		this.portfolioName = portfolioName;

		this.currentPrice = currentPrice;
	}

	public PortfolioRequestDto() {

	}

}
