package com.hcl.portfolio.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.hcl.portfolio.dto.PortfolioResponseDto;
import com.hcl.portfolio.exception.PortfolioException;
import com.hcl.portfolio.model.Portfolio;
import com.hcl.portfolio.service.IPortfolioService;

@RestController
public class PortfolioController {

	@Autowired
	IPortfolioService iPortfolioService;

	@GetMapping("/portfolios")
	public ResponseEntity<List<Portfolio>> getPortfolioList() {
		return new ResponseEntity<List<Portfolio>>(iPortfolioService.getPortfolio(), HttpStatus.OK);
	}

	@GetMapping("/portfolios/{portfolioId}")
	public ResponseEntity<PortfolioResponseDto> getPortfolioById(@PathVariable int portfolioId) throws PortfolioException
			 {
		return new ResponseEntity<PortfolioResponseDto>(iPortfolioService.searchPortfolioById(portfolioId),HttpStatus.OK);
	}

}
