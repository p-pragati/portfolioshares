package com.hcl.portfolio.serviceimpl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hcl.portfolio.dto.PortfolioResponseDto;
import com.hcl.portfolio.exception.PortfolioException;
import com.hcl.portfolio.model.Portfolio;
import com.hcl.portfolio.repository.PortfolioRepository;
import com.hcl.portfolio.service.IPortfolioService;

@Service
public class PortfolioServiceImpl implements IPortfolioService {

	public final static Logger logger = LoggerFactory.getLogger(PortfolioServiceImpl.class);
	@Autowired
	PortfolioRepository portfolioRepository;

	@Override
	public List<Portfolio> getPortfolio() {
		// TODO Auto-generated method stub
		String methodName = "getPortfolio()";
		logger.info(methodName + "called");
		return portfolioRepository.findAll();
	}

	@Override
	public PortfolioResponseDto searchPortfolioById(int portfolioId) throws PortfolioException  {
		String methodName = "searchPortfolioById()";
		logger.info(methodName + "called");
		Portfolio portfolios = new Portfolio();
		PortfolioResponseDto portfolioResponseDto = new PortfolioResponseDto();

		if (!portfolioRepository.existsById(portfolioId)) {
			logger.error("Portfolio Id doesnt exist");
			throw new PortfolioException("Portfolio Id doesnt exist");
		} else {

			portfolios = portfolioRepository.getById(portfolioId);
			BeanUtils.copyProperties(portfolios, portfolioResponseDto);
		}

		return portfolioResponseDto;

	}
}
