package com.hcl.portfolio.exception;

public class PortfolioException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	
	String message;


	public PortfolioException(String message) {
		super(message);
		this.message = message;
	}


	public PortfolioException() {
		super();
	}

}
