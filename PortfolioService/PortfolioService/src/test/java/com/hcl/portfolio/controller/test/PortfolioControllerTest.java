package com.hcl.portfolio.controller.test;

import static org.assertj.core.api.Assertions.assertThat;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.ResponseEntity;

import com.hcl.portfolio.controller.PortfolioController;
import com.hcl.portfolio.dto.PortfolioResponseDto;
import com.hcl.portfolio.exception.PortfolioException;
import com.hcl.portfolio.model.Portfolio;
import com.hcl.portfolio.serviceimpl.PortfolioServiceImpl;

@ExtendWith(MockitoExtension.class)
public class PortfolioControllerTest {
	@Mock
	PortfolioServiceImpl portfolioServiceImpl;
	@InjectMocks
	PortfolioController portfolioController;

	static Portfolio portfolio;
	static PortfolioResponseDto portfolioResponseDto;

	@BeforeAll
	public static void setUp() {
		portfolio = new Portfolio();
		portfolio.setPortfolioId(1);
		portfolio.setPortfolioName("Apple");

		portfolio.setCurrentPrice(45000.00);

		portfolioResponseDto = new PortfolioResponseDto();
		portfolioResponseDto.setPortfolioId(1);
		portfolioResponseDto.setPortfolioName("hcl");

		portfolioResponseDto.setCurrentPrice(45000.00);

	}

	@Test
	@DisplayName("SearchPortfolioById:Positive Scenario")
	public void testSearch() throws PortfolioException {
		PortfolioResponseDto portfolioList = new PortfolioResponseDto();
		// context
		when(portfolioServiceImpl.searchPortfolioById(1)).thenReturn(portfolioList);
		// event
		ResponseEntity<PortfolioResponseDto> list = portfolioController.getPortfolioById(1);
		// outcome
		assertEquals(portfolioList, list.getBody());
		assertThat(list.getStatusCodeValue()).isEqualTo(200);

	}

	@Test
	@DisplayName("SearchPortfolioById:Negative Scenario")
	public void testSearch1() throws PortfolioException {
		// context
		when(portfolioServiceImpl.searchPortfolioById(8)).thenThrow(PortfolioException.class);
		// event
		// outcome
		assertThrows(PortfolioException.class, () -> portfolioController.getPortfolioById(8));

	}
}
