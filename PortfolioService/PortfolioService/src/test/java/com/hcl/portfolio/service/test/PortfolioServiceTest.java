package com.hcl.portfolio.service.test;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.junit.jupiter.api.Assertions.assertThrows;

import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.BeanUtils;

import com.hcl.portfolio.dto.PortfolioResponseDto;
import com.hcl.portfolio.exception.PortfolioException;
import com.hcl.portfolio.model.Portfolio;
import com.hcl.portfolio.repository.PortfolioRepository;
import com.hcl.portfolio.serviceimpl.PortfolioServiceImpl;

@ExtendWith(MockitoExtension.class)
public class PortfolioServiceTest {
	@Mock
	PortfolioRepository portfolioRepository;
	@InjectMocks
	PortfolioServiceImpl portfolioServiceImpl;

	static Portfolio portfolio;

	static PortfolioResponseDto portfolioResponseDto;
	static PortfolioResponseDto portfolioResponseDto1;

	@BeforeAll
	public static void setUp() {
		portfolio = new Portfolio();
		portfolio.setPortfolioId(1);
		portfolio.setPortfolioName("Apple");

		portfolio.setCurrentPrice(45000.00);

		portfolioResponseDto = new PortfolioResponseDto();
		portfolioResponseDto.setPortfolioId(1);
		portfolioResponseDto.setPortfolioName("hcl");

		portfolioResponseDto.setCurrentPrice(45000.00);

	}

	@Test
	@DisplayName("search portfolio:positive scenerio")
	public void searchTest() throws PortfolioException {

		// context
		when(portfolioRepository.existsById(1)).thenReturn(true);
		// event
		BeanUtils.copyProperties(portfolio, portfolioResponseDto);
		PortfolioResponseDto result = portfolioServiceImpl.searchPortfolioById(1);
		// outcome
		assertEquals(true, result);
	}

	@Test
	@DisplayName("search portfolio:Negative scenerio")
	public void testSearch() throws PortfolioException {

		// context
		when(portfolioRepository.existsById(8)).thenReturn(false);
		// event
		// outcome
		assertThrows(PortfolioException.class, () -> portfolioServiceImpl.searchPortfolioById(8));

	}

}
